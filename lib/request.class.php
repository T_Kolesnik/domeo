<?php

class Request{
    
    
    public $post;
    public $server;
    public $get;
    public $files;
    
    
    
    public function __construct(){
        
        
     $this->post=$_POST;   
     $this->server=$_SERVER;
     $this->get=$_GET;
     $this->files=$_FILES;
        
    }
    
    
    public function getPost(){
        
        
     return isset ($this->post)? $this->post :null;  
        
        
        
    }
    
     public function get($key,$default=null){
        
        
     return isset ($this->get[$key])? $this->get[$key] :$default;  
        
        
        
    }
    
    public function getFilesKey($key,$default=null){
        
        
     return isset ($this->files[$key])? $this->files[$key] :$default;  
        
        
        
    }
    public function getPostKey($key,$default=null){
        
        
     return isset ($this->post[$key]) ? $this->post[$key] :$default;  
        
        
        
    }
    
    
    
    public function getMethod() {
        
        
      return strtolower($this->server['REQUEST_METHOD']);  
        
        
    }
    
    public function getHost() {
        
        
      return $this->server['SERVER_NAME'];  
        
        
    }
    
    
    
   public function isPost(){
       
       
    return $this->getMethod()=='post' ;  
       
       
       
   }
    
    
   public function getUri() {
    
    
    return $this->server['REQUEST_URI'];
    
   }
    
   
   public function mergeGET($params=array()) {
    
    $this->get+=$params;
    
      $_GET += $params;

    
    
   }
  
   public function mergePOST($params=array()) {
    
    $this->post+=$params;
    
      $_POST += $params;

    
    
   }  
}