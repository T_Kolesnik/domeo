<?php



class RepasitoryManager{
    
    
 private $repasitories=array();
 private $db_connection;
 
 
 public function __construct(DB $db){
  
  $this->db_connection=$db;
  return $this;
  
 }
 
 
 public function set($key,$value){
     
   $repasitories[$key]=$value;  
     
 }
    
 
 public function has($key){
     
     
  return isset($repasitories[$key]) ;  
     
 }
 
 
 public function get($key){
     
     
     if($this->has($key)){
         
        return $repasitories[$key] ;
         
     }
     
  $model_path = ROOT.DS.'models'.DS.strtolower($key).'.php';
  
 
  
  
  if(!file_exists($model_path)){
   
   throw new \Exeption("$key.Model not found");
   
  }
  
  $class=strtolower($key);
  
  
  
 $repo_class= new $class($this->db_connection);
 
 $this->set($key,$repo_class);
 
  
 return $repo_class; 
     
     
 }
    
    
}