<?php

class CustomersController extends Controller{

    public function __construct($data = array()){
        parent::__construct($data);
        $this->model = new Customer();
    }

    public function index(){
        
        
        if ( $_POST ){
            if ( $this->model->save($_POST) ){
                Session::setFlash('Thank you! Your message was sent successfully!');
            }
        }
    }

    public function admin_index(Request $request){
        
        // Пользователь Анна
        
        
        
        $page=(int)$request->get('page');
        if($page) { $currentPage=$page;} else {$currentPage=1;};
        $perPage=20;//todo config
        $countItems=count($this->model->getListCustomer($id));
        
        $this->data['customers'] = $this->model->getListCustomer($id,$currentPage,$perPage);
        
        foreach($this->data['customers'] as $key=>$customers){
            
           
           $id=unserialize($this->data['customers'][$key]['products']);
           
          $this->data['customers'][$key]['comments']= $this->container->get('repasitory_man')->get('Customer')->countCommentsCustomer($this->data['customers'][$key]['customer_id']);
           
           $name="";
           
           if(is_array($id)){
               
           foreach($id as $temp){ 
               
           $product=$this->container->get('repasitory_man')->get('Product')->getProduct($temp);
           
           $name.="<p><a href=\"/product/".$product[0]['id']."\">".$product[0]['name']."(".$product[0]['brand_name'].")"."</a></p>";
           
           }
           
           $this->data['customers'][$key]['products']=$name;
           }
           
          $this->data['customers'][$key]['products']=$name; 
        }
        
          
          $this->data['buttons']= (new Pagination($countItems,$perPage,$currentPage))->buttons; 
        
        
        
    }
    
    
    public function admin_my(){
        
        
        
    }



   public function admin_managers(){
        
        
        
    }
}