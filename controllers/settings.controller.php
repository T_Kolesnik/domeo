<?php

class SettingsController extends Controller{

    public function __construct($data = array()){
        parent::__construct($data);
        $this->model = new Setting();
    }
    
public function admin_project(){
    
 $this->data['project']=$this->container->get('repasitory_man')->get('Setting')->getListProject();   
    
}    
 
public function admin_project_edit(Request $request){
    
    
    
 if(!is_null($request->getPostKey('id'))){   
  
 $result=$this->container->get('repasitory_man')->get('Setting')->saveProject($request->post) ;
 
 if ( $result ){
                Session::setFlash('Page was saved.');
            } else {
                Session::setFlash('Error.');
            }
    
 } 
 
 $id=$this->params[0];   
 $this->data['project']=$this->container->get('repasitory_man')->get('Setting')->getProject($id);
    
}   
 
public function admin_project_add(Request $request){
    
 if($request->isPost()){   
  
 $result=$this->container->get('repasitory_man')->get('Setting')->saveProject($request->post) ; 
 
 if ( $result ){
                Session::setFlash('Page was saved.');
            } else {
                Session::setFlash('Error.');
            }
    
 } 
 
   
    
} 


public function admin_seo(){
    
$this->data['seo']=$this->container->get('repasitory_man')->get('Setting')->getSeo();       
    
}     
 
public function admin_editmainpage(){
    
    
    
}

public function admin_brand_add(){
    
    
    
}

public function admin_showbrands(){
    
$this->data['brands']=$this->container->get('repasitory_man')->get('Setting')->getListBrands();       
    
}

public function admin_style_add(){
    
    
    
}

public function admin_showstyles(){
    
$this->data['styles']=$this->container->get('repasitory_man')->get('Setting')->getListStyles();       
    
}

public function admin_category_add(){
    
    
    
}

public function admin_showcategorys(){
    
 $this->data['categorys']=$this->container->get('repasitory_man')->get('Setting')->getListCategorys();     
    
} 

public function admin_product_add(){
    
    
    
}

public function admin_showproducts(Request $request){
    
   
  
   $page=(int)$request->get('page');
        if($page) { $currentPage=$page;} else {$currentPage=1;};
        $perPage=20;//todo config
        $countItems=count($this->container->get('repasitory_man')->get('Product')->getListProducts($id));
        
        $this->data['products']=$this->container->get('repasitory_man')->get('Product')->getListProducts($id,$currentPage,$perPage); 
        $this->data['buttons']= (new Pagination($countItems,$perPage,$currentPage))->buttons; 
    
} 

 


public function admin_news_add(){
    
    
    
}

public function admin_shownews(){
    
$this->data['news']=$this->container->get('repasitory_man')->get('Setting')->getListNews();     
    
} 

public function admin_content_add(){
    
    
    
}

public function admin_showcontents(){
    
$this->data['contents']=$this->container->get('repasitory_man')->get('Setting')->getListContents();      
    
} 


public function admin_menu_add(){
    
    
    
}

public function admin_showmenu(){
    
$this->data['menu']=$this->container->get('repasitory_man')->get('Setting')->getListMenu();    
    
}

public function admin_editadmin(){
    
    
    
}
    
}