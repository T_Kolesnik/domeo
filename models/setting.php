<?php

class Setting extends Model {
    
    
 public function getListProject(){
   
   $sql="select * from la_projects";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }   
 
 public function getProject($id){
  
  
  
 $sql="select *, DATE_FORMAT(start, '%Y-%m-%d') as start , DATE_FORMAT(finish, '%Y-%m-%d') as finish from la_projects where id='{$id}'";
   
   $result=$this->db->query($sql);
   
   
     
    return $result[0]; 
    
    
  
 }
 
 public function saveProject($data=array()){
  
        $id = (int)$data['id'];
        $name = $this->db->escape($data['name']);
        $minidescr	 = $this->db->escape($data['short_content']);
        $descr = $this->db->escape($data['content']);
        $start = $data['start'];
        $finish = $data['finish'];
        $photo = $data['photo'];
        $updated= (new \DateTime)->format('Y-m-d H:i:s');

        if ( !$id ){ // Add new record
            $sql = "
                insert into la_projects
                   set name = '{$name}',
                       minidescr = '{$minidescr}',
                       descr = '{$descr}',
                       start = '{$start}',
                       finish = '{$finish}',
                       photo = '{$photo}',
                       updated = '{$updated}'
            ";
        } else { // Update existing record
            $sql = "
                update la_projects
                  set name = '{$name}',
                       minidescr = '{$minidescr}',
                       descr = '{$descr}',
                       start = '{$start}',
                       finish = '{$finish}',
                       photo = '{$photo}',
                       updated = '{$updated}'
                   where id = {$id}
            ";
        }



        return $this->db->query($sql);
 
 
 
  
  
 }
 
  public function getSeo(){
   
   $sql="select * from seo_optimise";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }      
 
  public function getListBrands(){
   
   $sql="select * from la_brand";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }      
 

 
 public function  getListStyles(){
   
   $sql="select * from la_style";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }   
 
 
 public function  getListCategorys(){
   
   $sql="select * from la_category";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }       
 
  public function  getListNews(){
   
   $sql="select * from la_news";
   
   $result=$this->db->query($sql);
     
    return $result; 
 }       
  
 public function getListContents(){
  
  $sql="select * from la_content";
   
   $result=$this->db->query($sql);
     
    return $result;  
  
  
  
 } 
 
 public function getListMenu(){
  
  $sql="select * from la_menu";
   
   $result=$this->db->query($sql);
     
    return $result; 
  
 }  
   
}